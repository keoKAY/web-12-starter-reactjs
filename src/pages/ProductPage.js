import React from "react";
import ProductCard from "../components/ProductCard";
// rafce
const ProductPage = () => {
  // list product information

  let productList = [
    {
      productTitle: "New  Iphone 14",
      description: "Here is the new iphone14 Description",
      image:
        "https://i.pinimg.com/736x/73/b4/9f/73b49fd3b3fc17e4acd85c6c2b90385a.jpg",
    },
    {
      productTitle: "New Ipad ",
      description: "Here is the ipad description ",
      image:
        "https://i.pinimg.com/originals/c5/b2/04/c5b204ce316d7e88c97440adb7972e3b.jpg",
    },
    {
      productTitle: "New AirPod",
      description: "Here ist eh description for the new airpod",
      image:
        "https://i.pinimg.com/originals/80/17/84/801784d8a63d5dfe9595bdd78516f5cf.jpg",
    },
    {
      productTitle: "New AirPod",
      description: "Here ist eh description for the new airpod",
      image:
        "https://i.pinimg.com/originals/80/17/84/801784d8a63d5dfe9595bdd78516f5cf.jpg",
    },
    {
      productTitle: "New AirPod",
      description: "Here ist eh description for the new airpod",
      image:
        "https://i.pinimg.com/originals/80/17/84/801784d8a63d5dfe9595bdd78516f5cf.jpg",
    },
    {
        productTitle: "New AirPod",
        description: "Here ist eh description for the new airpod",
        image:
          "https://i.pinimg.com/originals/80/17/84/801784d8a63d5dfe9595bdd78516f5cf.jpg",
      },
  ];

  // Create a grid of product
  return (
    <div className="container">
      <div className="row">
        {productList.map((pro) => (
          <div className="col-4">
            <ProductCard productInfo={pro} />
          </div>
        ))}
      </div>
    </div>
  );
};

export default ProductPage;
