import React, { Component } from "react";

export class Counter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: 0,
    };
    // this.handleIncrease = this.handleIncrease.bind(this);
  }
  //   handleIncrease= function (){
  //      this.setState({
  //         counter: this.state.counter + 1,
  //       });
  //   }

  render() {
    return (
      <div>
        <h1 className="text-center"> {this.state.counter}</h1>
        <div className="text-center">
          <button
            className="btn btn-warning"
            onClick={() => {
              this.setState({
                counter: this.state.counter + 1,
              });
            }}
          >
            {" "}
            Increase
          </button>
          <button
            disabled={ this.state.counter>0? false : true }
            className="btn btn-danger"
            onClick={() => {
              this.setState({
                counter: this.state.counter - 1,
              });
            }}
          >
            Decrease
          </button>
        </div>
      </div>
    );
  }
}
export default Counter;
