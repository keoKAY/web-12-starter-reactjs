import CustomNavBar from "./components/CustomNavBar";
import ProductCard from "./components/ProductCard";
import logo from "./logo.svg";
import Counter from "./pages/Counter";
import "bootstrap/dist/css/bootstrap.min.css";
import ProductPage from "./pages/ProductPage";
function App() {
  return (
    <>
      <CustomNavBar />
      <ProductPage />
    </>
  );
}

export default App;
