import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

function ProductCard(prop) {
    console.log("**** Props : " , prop)
  return (
    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src={prop.productInfo.image} />
      <Card.Body>
        <Card.Title> {prop.productInfo.productTitle}</Card.Title>
        <Card.Text>
          {prop.productInfo.description}
        </Card.Text>
        <Button variant="primary">Go somewhere</Button>
      </Card.Body>
    </Card>
  );
}

export default ProductCard;